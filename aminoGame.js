// canvas related variables
let canvas = document.getElementById("canvas");
let ctx = canvas.getContext("2d");

// variables used to get mouse position on the canvas
let $canvas = $("#canvas");
let canvasOffset = $canvas.offset();
let offsetX = canvasOffset.left;
let offsetY = canvasOffset.top;
let scrollX = $canvas.scrollLeft();
let scrollY = $canvas.scrollTop();

// variables to save last mouse position
// used to see how far the user dragged the mouse
// and then move the text by that distance
let startX;
let startY;

// this variable will hold the index of the hit-selected text
let selectedText = -1;

let aminoThreeLetter = ["GLY", "ALA", "VAL", "LEU", "ILE", "MET", "PHE", "TRP", "PRO",
                    "SER", "THR", "CYS", "TYR", "ASN", "GLN", "ASP", "GLU", 
                    "LYS", "ARG", "HIS"];
let aminoSingleLetter = ["G", "A", "V", "L", "I", "M", "F", "w", "P", "S", "T", "C", "Y", "N",
                        "Q", "D", "E", "K", "R", "H"];
let aminoNames = ["Glycine", "Alanine", "Valine", "Leucine", "Isoleucine", "Methionine",
                     "Fenylalanine", "Tryptofaan", "Proline", "Serine", "Threonine", "Cysteine",
                      "Tyrosine", "Asparagine", "Glutamine", "Aspartaat", "Glutamaat",
                       "Lysine", "Arginine", "Histidine"];
let aminoClasses = ["Non Polair", "Non Polair", "Non Polair", "Non Polair", "Non Polair",
                     "Non Polair", "Non Polair", "Non Polair", "Non polair", "Polair", "Polair", "Polair",
                      "Polair", "Polair", "Polair", "Zuur", "Zuur", "Basisch", "Basisch", "Basisch"];  

let myGameComponent;  
  
function startGame() {  
    let aminoAcidObjectList = createAminoAcidObjects();
    drawBackBone();  
    let randomAminoAcid = (aminoAcidObjectList[Math.floor(Math.random() * 21) ])
    console.log(randomAminoAcid)
    myGameComponent = new component(randomAminoAcid); //Random int from 0 till 20 
}  

function drawBackBone() {
    //clear the canvas 
    
    ctx.clearRect(0, 0, canvas.width, canvas.height); 
    ctx.font = "30px Arial";
    ctx.textBaseline = "middle";
    ctx.textAlign = "center";
    ctx.fillText(currentAminoAcid.name, myGameArea.canvas.width/2, myGameArea.canvas.height/11);
    ctx.fillText("+H3N --- C --- COO-", myGameArea.canvas.width/2, myGameArea.canvas.height/5);
    ctx.fillText("|", myGameArea.canvas.width/2, myGameArea.canvas.height/4);
}

  
// function component(currentAminoAcid) { 

//     this.update = function() {   
//         ctx = myGameArea.context;  
//         ctx.font = "30px Arial";
//         ctx.textBaseline = "middle";
//         ctx.textAlign = "center";
//         ctx.fillText(currentAminoAcid.name, myGameArea.canvas.width/2, myGameArea.canvas.height/11);
//         ctx.fillText("+H3N --- C --- COO-", myGameArea.canvas.width/2, myGameArea.canvas.height/5);
//         ctx.fillText("|", myGameArea.canvas.width/2, myGameArea.canvas.height/4);

//         // ctx.fillText("H", myGameArea.canvas.width/30, myGameArea.canvas.height/6);
//         // ctx.fillText("NH", myGameArea.canvas.width/30, myGameArea.canvas.height/4.5);
//         // ctx.fillText("NH2", myGameArea.canvas.width/30, myGameArea.canvas.height/3.5);
//         // ctx.fillText("NH2-", myGameArea.canvas.width/30, myGameArea.canvas.height/3);
//         // ctx.fillText("NH3+", myGameArea.canvas.width/30, myGameArea.canvas.height/2.5);
//         // ctx.fillText("OH", myGameArea.canvas.width/30, myGameArea.canvas.height/2);
//         // ctx.fillText("SH", myGameArea.canvas.width/30, myGameArea.canvas.height/1.5);

//         // ctx.fillText("CH", myGameArea.canvas.width/1.08, myGameArea.canvas.height/6);
//         // ctx.fillText("CH2", myGameArea.canvas.width/1.08, myGameArea.canvas.height/4.5);
//         // ctx.fillText("CH3", myGameArea.canvas.width/1.08, myGameArea.canvas.height/3.5);
//         // ctx.fillText("COO-", myGameArea.canvas.width/1.08, myGameArea.canvas.height/3);
//         // ctx.fillText("CH kring", myGameArea.canvas.width/1.08, myGameArea.canvas.height/2.5);
//         // ctx.drawImage(currentAminoAcid.structure, 10, 10);

//     }  
// }  
  

function createAminoAcidObjects() {
        let aminoAcidObjectList = [];
        for (let i = 0; i <= 19; i++) {
            let currentThreeLetter = aminoThreeLetter[i]
            currentThreeLetter = new Object(); 
            currentThreeLetter.name = aminoNames[i];
            currentThreeLetter.singleLetter = aminoSingleLetter[i];
            currentThreeLetter.class = aminoClasses[i];
            currentThreeLetter.threeLetter = aminoThreeLetter[i];
            currentThreeLetter.structure = "aminoStructures/" + aminoNames[i].toLowerCase() + ".png";
            
            aminoAcidObjectList.push(currentThreeLetter);
        }

        return aminoAcidObjectList;
    }
    
// handle mousedown events
// iterate through texts[] and see if the user
// mousedown'ed on one of them
// If yes, set the selectedText to the index of that text
function handleMouseDown(e) {
    e.preventDefault();
    startX = parseInt(e.clientX - offsetX);
    startY = parseInt(e.clientY - offsetY);
    // Put your mousedown stuff here
    for (var i = 0; i < texts.length; i++) {
        if (textHittest(startX, startY, i)) {
            selectedText = i;
        }
    }
}

// done dragging
function handleMouseUp(e) {
    e.preventDefault();
    selectedText = -1;
}

// also done dragging
function handleMouseOut(e) {
    e.preventDefault();
    selectedText = -1;
}
                    
// listen for mouse events
$("#canvas").mousedown(function (e) {
    handleMouseDown(e);
});
$("#canvas").mousemove(function (e) {
    handleMouseMove(e);
});
$("#canvas").mouseup(function (e) {
    handleMouseUp(e);
});
$("#canvas").mouseout(function (e) {
    handleMouseOut(e);
});